package com.blazemeter.demo;


import java.io.*;
import java.util.Properties;

/**
 * @author Crunchify.com
 *
 */

public class CrunchifyGetPropertyValues {
    String result = "";
    InputStream inputStream;

    public InputStream getPropValues() throws IOException {

        try {
            Properties prop = new Properties();
            File jarPath=new File(CrunchifyGetPropertyValues.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            String propertiesPath=jarPath.getParentFile().getAbsolutePath();
            System.out.println(" propertiesPath-"+propertiesPath);
            FileInputStream fileInputStream = new FileInputStream(propertiesPath + "/config.properties");

            return  fileInputStream;
//            String propFileName = "config.properties";
//
//            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);


        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return inputStream;

    }
}