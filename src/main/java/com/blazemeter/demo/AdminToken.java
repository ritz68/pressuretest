package com.blazemeter.demo;

import com.google.gson.Gson;
import com.squareup.okhttp.*;

import java.io.IOException;


public class AdminToken {

    public static String getToken(String url) throws IOException {

        Gson gson = new Gson();

        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n\t\"userName\":\"guest\",\n\t\"password\":\"guest123\"\n}");

        Request request = new Request.Builder()
                .url("https://" + url + "/cybershop/v2/api/cybershop/catalog/guest/login")
                .method("POST", body)
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();

        String string = response.body().string();

        AuthDTO authDTO = gson.fromJson(string, AuthDTO.class);
        System.out.println(authDTO.toString());


        return authDTO.getAccessToken();
    }
}
