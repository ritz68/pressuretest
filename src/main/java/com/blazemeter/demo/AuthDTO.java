package com.blazemeter.demo;

public class AuthDTO {
    private String username;
    private String accessToken;
    private String refereshToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefereshToken() {
        return refereshToken;
    }

    public void setRefereshToken(String refereshToken) {
        this.refereshToken = refereshToken;
    }

    @Override
    public String toString() {
        return "AuthDTO{" +
                "username='" + username + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", refereshToken='" + refereshToken + '\'' +
                '}';
    }
}
