package com.blazemeter.demo;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.control.Authorization;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.control.gui.HttpTestSampleGui;
import org.apache.jmeter.protocol.http.gui.HeaderPanel;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.http.util.HTTPArgument;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JmeterRunner {

    public static  void getRuuner(String domain, String path, String name, String token, int loop, int numThread) throws IOException {

        StandardJMeterEngine jmeter = new StandardJMeterEngine();
        Properties prop = new Properties();

        InputStream inputStream = new CrunchifyGetPropertyValues().getPropValues();
        prop.load(inputStream);

        String jmeterHome = prop.getProperty("JmeterHome");
        JMeterUtils.loadJMeterProperties(jmeterHome);

        JMeterUtils.initLocale();
        Authorization authorization = new Authorization();


        HashTree testPlanTree = new HashTree();

        HTTPSamplerProxy examplecomSampler = new HTTPSamplerProxy();
        examplecomSampler.setDomain(domain);
        examplecomSampler.setPort(443);
        examplecomSampler.setPath(path);
        examplecomSampler.setMethod("GET");
        examplecomSampler.setName(name);
        examplecomSampler.setProtocol("https");
        examplecomSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
        examplecomSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());

        HeaderManager manager = new HeaderManager();
        manager.add(new Header("Content-Type", "application/json"));
        manager.add(new Header("From", "orderGateway"));
        manager.add(new Header("Accept", "application/json"));
        manager.add(new Header("Authorization", "Bearer "+token));

        manager.setName(JMeterUtils.getResString("header_manager_title")); // $NON-NLS-1$
        manager.setProperty(TestElement.TEST_CLASS, HeaderManager.class.getName());
        manager.setProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName());


        LoopController loopController = new LoopController();
        loopController.setLoops(loop);
        loopController.setFirst(true);
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();

        ThreadGroup threadGroup = new ThreadGroup();
        threadGroup.setName("CyberDesign Thread Group");
        threadGroup.setNumThreads(numThread);
        threadGroup.setRampUp(1);
        threadGroup.setSamplerController(loopController);
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

        TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());

        testPlanTree.add(testPlan);
        HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
        threadGroupHashTree.add(examplecomSampler, manager);

//        SaveService.saveTree(testPlanTree, new FileOutputStream( "/bin/test.jmx"));


        //add Summarizer output to get test progress in stdout like:
        // summary =      2 in   1.3s =    1.5/s Avg:   631 Min:   290 Max:   973 Err:     0 (0.00%)
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }


        // Store execution results into a .jtl file
        String logFile = "result.jtl";
        ResultCollector logger = new ResultCollector(summer);
        logger.setFilename(logFile);
        testPlanTree.add(testPlanTree.getArray()[0], logger);

        // Run Test Plan
        jmeter.configure(testPlanTree);
        jmeter.run();

        System.out.println("Test completed. See example.jtl file for results");
        System.out.println("JMeter .jmx script is available at result.jmx");
    }




    public static  void deleteRuuner(String domain,String path, String name, String token, int loop, int numThread) throws IOException {

        StandardJMeterEngine jmeter = new StandardJMeterEngine();
        Properties prop = new Properties();

        InputStream inputStream = new CrunchifyGetPropertyValues().getPropValues();
        prop.load(inputStream);

        String jmeterHome = prop.getProperty("JmeterHome");
        JMeterUtils.loadJMeterProperties(jmeterHome);

        JMeterUtils.initLocale();
        Authorization authorization = new Authorization();


        HashTree testPlanTree = new HashTree();

        HTTPSamplerProxy examplecomSampler = new HTTPSamplerProxy();
        examplecomSampler.setDomain(domain);
        examplecomSampler.setPort(443);
        examplecomSampler.setPath(path);
        examplecomSampler.setMethod("DELETE");
        examplecomSampler.setName(name);
        examplecomSampler.setProtocol("https");
        examplecomSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
        examplecomSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());

        HeaderManager manager = new HeaderManager();
        manager.add(new Header("Content-Type", "application/json"));
        manager.add(new Header("From", "orderGateway"));
        manager.add(new Header("Accept", "application/json"));
        manager.add(new Header("Authorization", "Bearer "+token));

        manager.setName(JMeterUtils.getResString("header_manager_title")); // $NON-NLS-1$
        manager.setProperty(TestElement.TEST_CLASS, HeaderManager.class.getName());
        manager.setProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName());


        LoopController loopController = new LoopController();
        loopController.setLoops(loop);
        loopController.setFirst(true);
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();

        ThreadGroup threadGroup = new ThreadGroup();
        threadGroup.setName("CyberDesign Thread Group");
        threadGroup.setNumThreads(numThread);
        threadGroup.setRampUp(1);
        threadGroup.setSamplerController(loopController);
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

        TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());

        testPlanTree.add(testPlan);
        HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
        threadGroupHashTree.add(examplecomSampler, manager);

//        SaveService.saveTree(testPlanTree, new FileOutputStream( "/bin/test.jmx"));


        //add Summarizer output to get test progress in stdout like:
        // summary =      2 in   1.3s =    1.5/s Avg:   631 Min:   290 Max:   973 Err:     0 (0.00%)
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }


        // Store execution results into a .jtl file
        String logFile = "result.jtl";
        ResultCollector logger = new ResultCollector(summer);
        logger.setFilename(logFile);
        testPlanTree.add(testPlanTree.getArray()[0], logger);

        // Run Test Plan
        jmeter.configure(testPlanTree);
        jmeter.run();

        System.out.println("Test completed. See example.jtl file for results");
        System.out.println("JMeter .jmx script is available at result.jmx");
    }





    public static  void postRuuner(String domain,String path, String name, String token, int loop, int numThread,String body) throws IOException {

        StandardJMeterEngine jmeter = new StandardJMeterEngine();
        Properties prop = new Properties();

        InputStream inputStream = new CrunchifyGetPropertyValues().getPropValues();
        prop.load(inputStream);

        String jmeterHome = prop.getProperty("JmeterHome");
        JMeterUtils.loadJMeterProperties(jmeterHome);

        JMeterUtils.initLocale();
        Authorization authorization = new Authorization();


        HashTree testPlanTree = new HashTree();

        HTTPSamplerProxy examplecomSampler = new HTTPSamplerProxy();
        examplecomSampler.setDomain(domain);
        examplecomSampler.setPort(443);
        examplecomSampler.setPath(path);
        examplecomSampler.setMethod("POST");
        examplecomSampler.setName(name);
        examplecomSampler.setProtocol("https");

        HTTPArgument httpArgument = new HTTPArgument();
        httpArgument.setValue(body);

        examplecomSampler.addTestElement(httpArgument);
        examplecomSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
        examplecomSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());
        examplecomSampler.addNonEncodedArgument("body", body, "");
        examplecomSampler.setPostBodyRaw(true);

        HeaderManager manager = new HeaderManager();
        manager.add(new Header("Content-Type", "application/json"));
        manager.add(new Header("From", "orderGateway"));
        manager.add(new Header("Accept", "application/json"));
        manager.add(new Header("Authorization", "Bearer "+token));

        manager.setName(JMeterUtils.getResString("header_manager_title")); // $NON-NLS-1$
        manager.setProperty(TestElement.TEST_CLASS, HeaderManager.class.getName());
        manager.setProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName());


        LoopController loopController = new LoopController();
        loopController.setLoops(loop);
        loopController.setFirst(true);
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();

        ThreadGroup threadGroup = new ThreadGroup();
        threadGroup.setName("CyberDesign Thread Group");
        threadGroup.setNumThreads(numThread);
        threadGroup.setRampUp(1);
        threadGroup.setSamplerController(loopController);
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

        TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());

        testPlanTree.add(testPlan);
        HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
        threadGroupHashTree.add(examplecomSampler, manager);

//        SaveService.saveTree(testPlanTree, new FileOutputStream( "/bin/test.jmx"));


        //add Summarizer output to get test progress in stdout like:
        // summary =      2 in   1.3s =    1.5/s Avg:   631 Min:   290 Max:   973 Err:     0 (0.00%)
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }


        // Store execution results into a .jtl file
        String logFile = "result.jtl";
        ResultCollector logger = new ResultCollector(summer);
        logger.setFilename(logFile);
        testPlanTree.add(testPlanTree.getArray()[0], logger);

        // Run Test Plan
        jmeter.configure(testPlanTree);
        jmeter.run();

        System.out.println("Test completed. See example.jtl file for results");
        System.out.println("JMeter .jmx script is available at result.jmx");
    }
}

