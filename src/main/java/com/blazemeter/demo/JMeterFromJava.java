package com.blazemeter.demo;

import java.io.InputStream;
import java.util.Properties;


public class JMeterFromJava {

    public static void main(String[] args) throws Exception {


        Properties prop = new Properties();

        InputStream inputStream = new CrunchifyGetPropertyValues().getPropValues();
        prop.load(inputStream);

        String domain = prop.getProperty("domain");
        String loop = prop.getProperty("loop");
        String numThread = prop.getProperty("numThread");


        String token = AdminToken.getToken(domain);

        JmeterRunner.getRuuner(domain, Constant.GET_ALL_STORE_PATH, Constant.GET_ALL_STORE_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_ALL_MERCHANT_PATH, Constant.GET_ALL_MERCHANT_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_ALL_PRICE_DETAILS_PATH, Constant.GET_ALL_PRICE_DETAILS_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_PRODUCTS_BY_CATEGORY_ID_PATH, Constant.GET_PRODUCTS_BY_CATEGORY_ID__NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_STORE_BY_MERCHANT_ID_PATH, Constant.GET_STORE_BY_MERCHANT_ID_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_ALL_STORE_TYPE_PATH, Constant.GET_ALL_STORE_TYPE__NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.getRuuner(domain, Constant.GET_ALL_PAYMENT_TYPE_PATH, Constant.GET_ALL_PAYMENT_TYPE__NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.deleteRuuner(domain, Constant.DELETE_PRODUCT_PATH, Constant.DELETE_PRODUCT_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.deleteRuuner(domain, Constant.DELETE_CATEGORY_PATH, Constant.DELETE_CATEGORY_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.deleteRuuner(domain, Constant.DELETE_PAYMENT_TYPE_PATH, Constant.DELETE_PAYMENT_TYPE_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.deleteRuuner(domain, Constant.DELETE_STORE_PATH, Constant.DELETE_STORE_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.deleteRuuner(domain, Constant.DELETE_IMAGE_PATH, Constant.DELETE_IMAGE_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread));
        JmeterRunner.postRuuner(domain, Constant.ADD_PRODUCT_PATH, Constant.ADD_PRODUCT_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread), "{\n\t\"categoryId\":\"614\",\n\t\"productName\":\"Ritz Test\",\n\t\"productDescription\":\"Desc\",\n\t\"productCurrencyDetails\":\"{'s':'s'}\",\n\t\"productType\":\"HOME\",\n\t\"optionalDetails\":\"(OptionalTableId,  product Id, Key , value )\",\n\t\"customQRCode\":\"00000000\",\n\t\"vat\":\"1.5\",\n\t \"GTIN\": \"99\"\n\n}");
        JmeterRunner.postRuuner(domain, Constant.ADD_CATALOG_PATH, Constant.ADD_CATALOG_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread), "{\n\t\"categoryName\":\"Food\",\n\t\"categoryDescription\":\"FoodDesc\"\n}");
        JmeterRunner.postRuuner(domain, Constant.ADD_STORE_PATH, Constant.ADD_STORE_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread), "{\n\t\"storeName\":\"GoodIdea22222\",\n\t\"storeDescription\":\"GoodIdeaBranch\",\n\t\"latitude\":\"60000\",\n\t\"email\":\"silvertrail@stockholm.se\",\n\t\"phone\":\"012341\",\n\t\"storeActionTableDetails\":\"[{\\\"sequenceNo\\\":1,\\\"sequenceLabel\\\":\\\"Tack \\\",\\\"sequenceAction\\\":\\\"singlevibrate&ring\\\",\\\"storeId\\\":\\\"2667\\\",\\\"position\\\":{\\\"x\\\":-400,\\\"y\\\":32}},{\\\"sequenceNo\\\":99,\\\"sequenceLabel\\\":\\\"Finish\\\",\\\"sequenceAction\\\":\\\"vibrate&singlering\\\",\\\"storeId\\\":\\\"2667\\\",\\\"position\\\":{\\\"x\\\":0,\\\"y\\\":32}}]\",\n\t\"storeWorkingHoursDetails\":\"{'version':0,'ddsdsd':'sdsd'}}\",\n\t\"longitude\":\"80111\",\n\t\"address1\":\"taby\",\n\t\"address2\":\"street 2\",\n\t\"zipCode\":\"185 68\",\n\t\"city\":\"Stockholm\",\n\t\"baseLanguage\":\"EN\",\n\t\"country\":\"Sweden\",\n\t\"reciptsTemplate\":\"121212\",\n\t\"status\":\"open\",\n\t\"organizationNumber\":\"17870912\",\n\t\"merchantId\":\"2671\",\n\t\"storePayments\": [\n\t\t{\n\t\t\t\"paymentTypeId\":\"522\",\n\t\t\t\"name\":\"swishNumber\",\n\t\t\t\"value\":\"007\",\n\t\t\t\"swishPemFile\":\"TestFrmGatewa\"\n\t\t}\n\t\t],\n\t\t\"actionTable\":[\n\t\t\t{\n\t\t\t\"sequenceNo\":\"1\",\n\t\t\t\"sequenceLabel\":\"READY\",\n\t\t\t\"sequenceAction\":\"RING\"\n\t\t\t}\n\t\t],\n\t\t\n\t\t\"storeTypeDetails\":[\n\t\t\t{\n\t\t\t\"typeId\":\"463\"\n\t\t\t}\n\t\t]\n}");
/*
Order Gateway
 */



        JmeterRunner.postRuuner(domain, Constant.NEW_ORDER_PATH, Constant.NEW_ORDER_NAME, token, Integer.parseInt(loop), Integer.parseInt(numThread), "{\n" +
                "    \"catalogVersion\": 1,\n" +
                "    \"currency\": \"SEK\",\n" +
                "    \"deliveryAddress\": \"\",\n" +
                "    \"deliveryDesiredTime\": 1552300539354,\n" +
                "    \"deliveryType\": 0,\n" +
                "    \"flowType\": 0,\n" +
                "    \"latitude\": 0.0,\n" +
                "    \"longitude\": 0.0,\n" +
                "    \"merchantId\": \"1\",\n" +
                "    \"message\": \"\",\n" +
                "    \"notificationIds\": \"\",\n" +
                "    \"orderItems\": [\n" +
                "        {\n" +
                "            \"description\": \"Desc\",\n" +
                "            \"name\": \"Soda\",\n" +
                "            \"price\": [\n" +
                "                {\n" +
                "                    \"code\": \"SEK\",\n" +
                "                    \"value\": \"10.00\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"productId\": \"1306\",\n" +
                "            \"quantity\": 1\n" +
                "        }\n" +
                "    ],\n" +
                "    \"paymentGateway\": \"SWISH\",\n" +
                "    \"status\": 0,\n" +
                "    \"storeId\": \"1294\",\n" +
                "    \"total\": [\n" +
                "        {\n" +
                "            \"code\": \"SEK\",\n" +
                "            \"value\": \"10.00\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"unitName\": \"Cashier\",\n" +
                "    \"unitValue\": \"5\"\n" +
                "}");

    }
}