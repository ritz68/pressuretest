package com.blazemeter.demo;

public class Constant {

    public static final String GET_ALL_STORE_PATH="/cybershop/v2/api/cybershop/catalog/store/getAllStores";
    public static final String GET_ALL_STORE_NAME="Get All Store";


    public static final String GET_ALL_MERCHANT_PATH="/cybershop/v2/api/cybershop/catalog/merchant/getAllMerchants";
    public static final String GET_ALL_MERCHANT_NAME="Get All Merchant";

    public static final String GET_ALL_PRICE_DETAILS_PATH="/cybershop/v2/api/cybershop/catalog/product/getAllPrices";
    public static final String GET_ALL_PRICE_DETAILS_NAME="Get All Price Details";


    public static final String GET_PRODUCTS_BY_CATEGORY_ID_PATH="/cybershop/v2/api/cybershop/catalog/product/categoryId/1085";
    public static final String GET_PRODUCTS_BY_CATEGORY_ID__NAME="Get Products by CategoryId";


    public static final String GET_STORE_BY_MERCHANT_ID_PATH="/cybershop/v2/api/cybershop/catalog/store/merchant/1117";
    public static final String GET_STORE_BY_MERCHANT_ID_NAME="Get Store by MerchantId";


    public static final String GET_ALL_STORE_TYPE_PATH="/cybershop/v2/api/cybershop/catalog/storeType/getAllStoreType";
    public static final String GET_ALL_STORE_TYPE__NAME="Get All Store Type";



    public static final String GET_ALL_PAYMENT_TYPE_PATH="/cybershop/v2/api/cybershop/catalog/paymentType/getAllPaymentTypes";
    public static final String GET_ALL_PAYMENT_TYPE__NAME="Get All Payment Types";

    public static final String DELETE_PRODUCT_PATH="/cybershop/v2/api/cybershop/catalog/product/996";
    public static final String DELETE_PRODUCT_NAME="Delete Product Item";

    public static final String DELETE_CATEGORY_PATH="/cybershop/v2/api/cybershop/catalog/category/1957";
    public static final String DELETE_CATEGORY_NAME="Delete Category Item";


    public static final String DELETE_PAYMENT_TYPE_PATH="/cybershop/v2/api/cybershop/catalog/paymentType/434";
    public static final String DELETE_PAYMENT_TYPE_NAME="Delete Payment Type";

    public static final String DELETE_STORE_PATH="/cybershop/v2/api/cybershop/catalog/store/2682";
    public static final String DELETE_STORE_NAME="Delete Store";



    public static final String DELETE_IMAGE_PATH="/cybershop/v2/api/cybershop/catalog/images/1821";
    public static final String DELETE_IMAGE_NAME="Delete Image";



    public static final String ADD_PRODUCT_PATH="/cybershop/v2/api/cybershop/catalog/product/";
    public static final String ADD_PRODUCT_NAME="Add Product";


    public static final String ADD_CATALOG_PATH="/cybershop/v2/api/cybershop/catalog/category/parent/610";
    public static final String ADD_CATALOG_NAME="Add Category";



    public static final String ADD_STORE_PATH="/cybershop/v2/api/cybershop/catalog/store";
    public static final String ADD_STORE_NAME="Add Store";

    public static final String NEW_ORDER_PATH="/order/api/v2/cybershop/payment/command/request";
    public static final String NEW_ORDER_NAME="New Order";


}
